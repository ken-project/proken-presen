﻿<!DOCTYPE html>
<html>
<head>
<meta charset='utf-8'>
<title>テストサーバからの読み込み・変換</title>
</head>
<body>
<?php
//URLからサーバ上のjsonデータの読み取り
$contents = file_get_contents('https://proken-ui.herokuapp.com/api/show.json?classterId=1&periodId=1');
//echo mb_detect_encoding($contents);
//echo json_encode($contents);
echo $contents;
echo "<br><br>";

//jsonデータのプロパティ名の置換
$replaced = str_replace("contents", "children", $contents);
echo $replaced;
echo "<br><br>";

//jsonをファイルに出力
//mb_http_output("utf-8");
$filename = './replaced.json';
if (!file_exists($filename)) {
	touch($filename);
} else {
	echo ('すでにファイルが存在しています。file name:' . $filename);
}

if (!file_exists($filename) && !is_writable($filename)
	|| !is_writable(dirname($filename))) {
	echo "書き込みできないか、ファイルがありません。",PHP_EOL;
	exit(-1);
}

$fp = fopen($filename,'w') or dir('ファイルを開けません');

fwrite($fp, sprintf($contents));

fclose($fp);
?>
</body>
</html>

