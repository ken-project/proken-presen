﻿<!DOCTYPE html>
<html>
<head>
<meta charset='utf-8'>
<title>テストサーバからの読み込み・変換・書き出し</title>
</head>
<body>
<?php
//header("Content-type: application/x-javascript");//HTML上に出力させるためのタグ
//echo "document.write(\'success!!\')";
//echo '<br><br>';

//フォームの値の受け取り
$form_value = $_POST["period"];
echo "form_value = ".$form_value;


//URLからサーバ上のjsonデータの読み取り
//$contents = file_get_contents('https://proken-ui.herokuapp.com/api/show.json?periodId='.$form_value);
$contents = file_get_contents('https://proken-ui.herokuapp.com/api/show.json?periodId=1');
//echo mb_detect_encoding($contents);
//echo json_encode($contents);
echo $contents;
echo "<br><br>";

//jsonデータのプロパティ名の置換
if($replaced = str_replace("contents", "children", $contents)){
	echo 'contents → children';
}else{
	echo '置換失敗';
}
echo '<br><br>';
echo $replaced;
echo "<br><br>";

//中身(json)を連想配列に変換(第2引数にtrueを指定すると配列で返ってくる)
$json_array = json_decode($replaced,true);
echo "<pre>";
var_dump($json_array);
echo "</pre>";

//echo $n = count($json_array["children"][0]);
//echo '<br><br>';

//print_r($json_array);
//echo '<br><br>';

//echo $json_array["children"][0]["topic_name"];
//echo '<br><br>';

/*
//早稲田だったら加算...
$size = 0;
foreach($json_array as $valueA){
	foreach($valueA as $valueB){
		foreach($valueB as $valueC){
			var_dump($valueC);
			if($valueC != "早稲田"){
				break;
			}else{
				foreach($valueB as $key => $valueC){
					print('   ['.$key.']:'.$valueC);
					if($key == "topic_score"){
						echo '<br>';
						$size += $valueC;
						print('size : '.$size.'<br>');
					}
				}
			}
			//echo '<br>';
		}
		echo '<br>';
	}
}
*/

//配列の階層を変更、クラスタ名の下にトピック名やスコアを移す
$down_hi = [
    'children' => array_map(
        function ($arr) {
            $arr[] = array_splice($arr, 1);
            return $arr;
        },
        $json_array['children']
    ),
];
echo '<pre>';
var_dump($down_hi);
echo '</pre>';

//同じクラスタ名のものをまとめる
$collect_cl['children'] = array_values(array_reduce($down_hi["children"], function ($r, $a) {
    //$group = array_values($a)[0];
    $group = reset($a);
    if (isset($r[$group]) == false) {
        $r[$group] = $a;
    } else {
        $r[$group] = array_merge($r[$group], array_slice($a, 1));
    }
    return $r;
}, []));
echo '<pre>';
var_dump($collect_cl);
echo '</pre>';

//jsonに戻す
echo $json = json_encode($collect_cl,JSON_UNESCAPED_UNICODE);
echo "<br><br>";

//置換
$replaced2 = preg_replace('/"\d"/', '"children"', $json);
//echo $replaced2."<br><br>";
$replaced3 = preg_replace('/"children":{/', '"children":[{', $replaced2);
$replaced3 = preg_replace('/}}/', '}]}', $replaced3);
$replaced3 = preg_replace('/cluster_name/', 'topic_name', $replaced3);
$replaced3 = preg_replace('/},"children":\[{/', '},{', $replaced3);
echo $replaced3."<br><br>";

//jsonをファイルに出力
//mb_http_output("utf-8");
$filename = './test.json';
if (!file_exists($filename)) {
	touch($filename);
} else {
	echo ('すでにファイルが存在しています。file name:'.$filename.'<br>');
}

if (!file_exists($filename) && !is_writable($filename)
	|| !is_writable(dirname($filename))) {
	echo "書き込みできないか、ファイルがありません。",PHP_EOL;
	exit(-1);
}

$fp = fopen($filename,'w') or dir('ファイルを開けません');
if(fwrite($fp, sprintf($replaced3))){
	echo ($filename.'を更新しました。');
}
fclose($fp);

//include('waseda_keio.html');
//require("waseda_keio.html");
echo $html = file_get_contents("waseda_keio.html");

?>
