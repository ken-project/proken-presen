<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
    <link type="text/css" rel="stylesheet" href="style.css"/>
    <style type="text/css">

text {
  font-size: 20px;
  fill:red;
  pointer-events: none;
}

text.parent {
	font-size:40px;
	fill: white;
}

circle {
  fill: orange;
  stroke: orange;
  pointer-events: all;
}

circle.parent {
  fill: orange;
  fill-opacity: .12;
  stroke: orange;
}

circle.parent:hover {
  stroke: #ff7f0e;
  stroke-width: .5px;
}

circle.child {
  pointer-events: none;
}

a {
  position: relative;
  z-index: 777;
}

    </style>
    <!-- <title>つぶやきビッグデータ++</title> -->
    
  </head>
  <body>
  
    <!-- <a href="topic_test.html">テスト</a>にリンクしています。 -->
  <!--
  <h1 style="color:white;">つぶやきビッグデータ++</h1>
  <p style="color:white;">早稲田 <br />慶応<br />上智</p>
  -->
  
  <?php
//URLからサーバ上のjsonデータの読み取り
$contents = file_get_contents('https://proken-ui.herokuapp.com/api/show.json?classterId=1&periodId=1');
//echo mb_detect_encoding($contents);
//echo json_encode($contents);
echo $contents;
echo "<br><br>";

//jsonデータのプロパティ名の置換
$replaced = str_replace("contents", "children", $contents);
echo $replaced;
echo "<br><br>";

//jsonをファイルに出力
$filename = './replaced.json';
if (!file_exists($filename)) {
	touch($filename);
} else {
//	echo ('すでにファイルが存在しています。file name:' . $filename);
}

if (!file_exists($filename) && !is_writable($filename)
	|| !is_writable(dirname($filename))) {
	echo "書き込みできないか、ファイルがありません。",PHP_EOL;
	exit(-1);
}

$fp = fopen($filename,'w') or dir('ファイルを開けません');
fwrite($fp, sprintf($replaced));
fclose($fp);
?>
  
    <script type="text/javascript" src="d3/d3.js"></script>
    <script type="text/javascript" src="d3/d3.layout.js"></script>
    <script type="text/javascript" src="js/jQuery.js"></script>
    <script type="text/javascript">

var w = $(document).width(),
    h = $(document).height(),
    r = 720,
    x = d3.scale.linear().range([0, r]),
    y = d3.scale.linear().range([0, r]),
    node,
    root;

var pack = d3.layout.pack()
    .size([r, r])
    .value(function(d) { return d.topic_score; })

var vis = d3.select("body").insert("svg:svg", "h2")
    .attr("width", w)
    .attr("height", h)
  .append("svg:g")
    .attr("transform", "translate(" + (w - r) / 2 + "," + (h - r) / 2 + ")");

//json形式のファイルの読み込み
d3.json("replaced.json", function(data) {
  node = root = data;

  var nodes = pack.nodes(root);

  vis.selectAll("circle")
      .data(nodes)
    .enter().append("svg:circle")
      .attr("class", function(d) { return d.children ? "parent" : "child"; })
      .attr("cx", function(d) { return d.x; })
      .attr("cy", function(d) { return d.y; })
      .attr("r", function(d) { return d.r; })
      .on("click", function(d) { return zoom(node == d ? root : d); });

  vis.selectAll("text")
      .data(nodes)
    .enter().append("svg:text")
      .attr("class", function(d) { return d.children ? "parent" : "child"; })
      .attr("x", function(d) { return d.x; })
      .attr("y", function(d) { return d.y; })
      .attr("dy", ".35em")
      .attr("text-anchor", "middle")
      .style("opacity", function(d) { return d.r > 20 ? 1 : 0; })
      .text(function(d) { return d.topic_name; });

  d3.select(window).on("click", function() { zoom(root); });
});

function zoom(d, i) {
  var k = r / d.r / 2;
  x.domain([d.x - d.r, d.x + d.r]);
  y.domain([d.y - d.r, d.y + d.r]);

  var t = vis.transition()
      .duration(d3.event.altKey ? 7500 : 750);

  t.selectAll("circle")
      .attr("cx", function(d) { return x(d.x); })
      .attr("cy", function(d) { return y(d.y); })
      .attr("r", function(d) { return k * d.r; });

  t.selectAll("text")
      .attr("x", function(d) { return x(d.x); })
      .attr("y", function(d) { return y(d.y); })
      .style("opacity", function(d) { return k * d.r > 20 ? 1 : 0; });

  node = d;
  d3.event.stopPropagation();
}

    </script>
  </body>
</html>