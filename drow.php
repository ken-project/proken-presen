<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
    <link type="text/css" rel="stylesheet" href="style.css"/>
    <!--<link type="text/css" rel="stylesheet" href="css/bootstrap.min.css"/>-->

    <style type="text/css">
text {
  font-size: 20px;
  fill:red;
  pointer-events: none;
  font-family:'Lucida Grande','Hiragino Kaku Gothic ProN', 'ヒラギノ角ゴ ProN W3',Meiryo, メイリオ, sans-serif;
}

text.parent {
  font-size:40px;
  fill: white;
  font-family:'Lucida Grande','Hiragino Kaku Gothic ProN', 'ヒラギノ角ゴ ProN W3',Meiryo, メイリオ, sans-serif;
}

circle {
  fill: orange;
  stroke: orange;
  pointer-events: all;
}

circle.parent {
  fill: orange;
  fill-opacity: .12;
  stroke: orange;
}

circle.parent:hover {
  stroke: #ff7f0e;
  stroke-width: .5px;
}

circle.child {
  pointer-events: none;
}

a {
  position: relative;
  z-index: 777;
}

    </style>
    <!-- <title>つぶやきビッグデータ++</title> -->
    
  

	<meta charset='utf-8'>
	<title>描画</title>
	</head>
<body>
<?php
session_start();
ini_set( "display_errors", "Off");


//前回のファイルの削除
//$filename_old = './data/data_'.$_SESSION["period"].'.json';
if (file_exists($_SESSION["filename"])) {
	unlink($_SESSION["filename"]);
}


//フォームの値の受け取り
//periodの計算（次へ、前へに対応させる）
//$form_period = $_POST["period"];
//$period = $form_period;
$period_id = $_POST["period"];
//echo '$_POST["period"]'.$_POST["period"];
//echo "<br>";
echo '$_POST["cls_or_tpc"]'.$_POST["cls_or_tpc"];
echo "<br>";
echo '$_POST["prv_nxt"]'.$_POST["prv_nxt"];
echo "<br>";
if($_POST["prv_nxt"] != 0){//次へ、前へでの入力ならそれにあわせてperiodの値を増減して、セッションで値を保存
	$period_id = $_SESSION["period"] + $_POST["prv_nxt"];
	$_SESSION["period"] = $period_id;
}else{//次へ、前へでの入力でないならセッションで値を保存するだけ
	$_SESSION["period"] = $period_id;
}
echo '$_SESSION["period"]'.$_SESSION["period"];
echo "<br>";

$cluster = $_POST['cluster'];
//var_dump($cluster);

foreach($cluster as $cluster_id){
	echo $cluster_id;
	echo "<br>";
}


//入力に応じてサーバから必要なデータを取得
//クラスタを選択した場合
if($_POST["cls_or_tpc"]=="0"){
	$cluster_id = $_POST["cluster"];
	$_SESSION["cluster"] = $cluster_id;
	$_SESSION["cls_or_tpc"] = 0;
	
	$connect_cluster = '';
	$cls_or_tpc_id = '';
	foreach($cluster as $cluster_id){
		$each_cluster = file_get_contents('https://proken-ui.herokuapp.com/api/show.json?periodId='.$period_id.'&clusterId='.$cluster_id);
		$connect_cluster = $connect_cluster.$each_cluster;
		$cls_or_tpc_id = $cls_or_tpc_id.'.'.$cluster_id;
	}
	$contents = preg_replace('/]}{"contents":\[/', ',', $connect_cluster);
	//echo $cls_or_tpc_id;
	$cls_or_tpc = 0;
	$_SESSION["cls_or_tpc_id"] = $cls_or_tpc_id;
//トピックを選択した場合
}elseif($_POST["cls_or_tpc"]=="1"){
	$topic_id = $_POST["topic"];
	$_SESSION["topic"] = $topic_id;
	$_SESSION["cls_or_tpc"] = 1;
	$cls_or_tpc = 1;
	$cls_or_tpc_id = $topic_id;
	$contents = file_get_contents('https://proken-ui.herokuapp.com/api/show.json?periodId='.$period_id.'&topicId='.$topic_id);
//クラスタを選択した状態で「前へ」「次へ」ボタンを押した場合
}elseif($_SESSION["cls_or_tpc"]=="0"){
	$connect_cluster = '';
	$cls_or_tpc_id = '';
	foreach($_SESSION["cluster"] as $cluster_id){
		$each_cluster = file_get_contents('https://proken-ui.herokuapp.com/api/show.json?periodId='.$period_id.'&clusterId='.$cluster_id);
		$connect_cluster = $connect_cluster.$each_cluster;
		$cls_or_tpc_id = $cls_or_tpc_id.'.'.$cluster_id;
	}
	$contents = preg_replace('/]}{"contents":\[/', ',', $connect_cluster);
//トピックを選択した状態で「前へ」「次へ」ボタンを押した場合
}elseif($_SESSION["cls_or_tpc"]=="1"){
	$contents = file_get_contents('https://proken-ui.herokuapp.com/api/show.json?periodId='.$period_id.'&topicId='.$_SESSION["topic"]);
}else{
	echo "想定されていないケースです";
}




//URLからサーバ上のjsonデータの読み取り
//$contents = file_get_contents('https://proken-ui.herokuapp.com/api/show.json?periodId='.$period);
//$contents = file_get_contents('https://proken-ui.herokuapp.com/api/show.json?periodId=1');
//echo mb_detect_encoding($contents);
//echo json_encode($contents);
//echo $contents;
//echo "<br><br>";

//jsonデータのプロパティ名の置換
if($replaced = str_replace("contents", "children", $contents)){
	//echo 'contents → children';
}else{
	//echo '置換失敗';
}
//echo '<br><br>';
//echo $replaced;
//echo "<br><br>";

//中身(json)を連想配列に変換(第2引数にtrueを指定すると配列で返ってくる)
$json_array = json_decode($replaced,true);
//echo "<pre>";
//var_dump($json_array);
//echo "</pre>";

//echo $n = count($json_array["children"][0]);
//echo '<br><br>';

//print_r($json_array);
//echo '<br><br>';

//echo $json_array["children"][0]["topic_name"];
//echo '<br><br>';

//クラスタではなくトピックが選択された場合の処理
//トピックを先頭にもってくる
if($_POST["cls_or_tpc"]=="1" || ($_SESSION["cls_or_tpc"]=="1" && ($_POST["prv_nxt"]=="1" || $_POST["prv_nxt"]=="-1"))){
	foreach ($json_array["children"] as &$row) { 
		$row = array_merge(array_splice($row, 2, 1), $row); 
	}
	unset($row);
	$_SESSION["cls_or_tpc"] = "1";
	echo"aaaaaaaaaaaaa";
}
echo '$_SESSION["cls_or_tpc"]'.$_SESSION["cls_or_tpc"];
echo "<br>";
//$_SESSION["cls_or_tpc"] = "0";



//配列の階層を変更、クラスタ（トピック）の下にトピック（クラスタ）やスコアを移す
$down_hi = [
    'children' => array_map(
        function ($arr) {
            $arr[] = array_splice($arr, 1);
            return $arr;
        },
        $json_array['children']
    ),
];
//echo '<pre>';
//var_dump($down_hi);
//echo '</pre>';

//同じクラスタ名のものをまとめる
$collect_cl['children'] = array_values(array_reduce($down_hi["children"], function ($r, $a) {
    //$group = array_values($a)[0];
    $group = reset($a);
    if (isset($r[$group]) == false) {
        $r[$group] = $a;
    } else {
        $r[$group] = array_merge($r[$group], array_slice($a, 1));
    }
    return $r;
}, []));
//echo '<pre>';
//var_dump($collect_cl);
//echo '</pre>';

//jsonに戻す
$json = json_encode($collect_cl,JSON_UNESCAPED_UNICODE);
//echo $json;
//echo "<br><br>";

//置換
$replaced2 = preg_replace('/"\d"/', '"children"', $json);
//echo $replaced2."<br><br>";
$replaced3 = preg_replace('/"children":{/', '"children":[{', $replaced2);
$replaced3 = preg_replace('/}}/', '}]}', $replaced3);
$replaced3 = preg_replace('/cluster_name/', 'topic_name', $replaced3);
$replaced3 = preg_replace('/},"children":\[{/', '},{', $replaced3);
//echo $replaced3."<br><br>";

//整形したjsonデータをファイルに出力
//ファイル名 data_(日付)_(クラスタorトピック).(クラスタid orトピックid)
//mb_http_output("utf-8");
$filename = './data/data_'.$period_id.'_'.$cls_or_tpc.'_'.$cls_or_tpc_id.'.json';
if (!file_exists($filename)) {
	touch($filename);
} else {
	//echo ('すでにファイルが存在しています。file name:'.$filename.'<br>');
}
if (!file_exists($filename) && !is_writable($filename)
	|| !is_writable(dirname($filename))) {
	//echo "書き込みできないか、ファイルがありません。",PHP_EOL;
	exit(-1);
}
$fp = fopen($filename,'w') or dir('ファイルを開けません');
if(fwrite($fp, sprintf($replaced3))){
	echo ($filename.'を更新しました。');
	$_SESSION["filename"] = $filename;
}
fclose($fp);
?>

<h1 style="color:white;">
	<?php
		if(isset($_POST["cls_or_tpc"]) && $_POST["cls_or_tpc"]=="1"){
			if ($period_id == 1) echo "10/1 - 10/7";
			elseif ($period_id == 2) echo "10/8 - 10/14";
			elseif ($period_id === NULL) echo "つぶやきビッグデータ++";
			else echo "No data";
		}else{
			if ($period_id == 1) echo "10/1 - 10/7　大学生";
			elseif ($period_id == 2) echo "10/8 - 10/14　大学生";
			elseif ($period_id === NULL) echo "つぶやきビッグデータ++";
			else echo " No data";
		}
	?>
</h1>

  
    <script type="text/javascript" src="d3/d3.js"></script>
    <script type="text/javascript" src="d3/d3.layout.js"></script>
    <script type="text/javascript" src="js/jQuery.js"></script>
    <script type="text/javascript">

var w = $(document).width(),
    h = $(document).height(),
    r = 720,
    x = d3.scale.linear().range([0, r]),
    y = d3.scale.linear().range([0, r]),
    node,
    root;

var pack = d3.layout.pack()
    .size([r, r])
    .value(function(d) { return d.topic_score; })

var vis = d3.select("body").insert("svg:svg", "h2")
    .attr("width", w)
    .attr("height", h)
  .append("svg:g")
    .attr("transform", "translate(" + (w - r) / 2 + "," + (h - r) / 2 + ")");

//var json_text = '{"children":[{"cluster_name":"早稲田","cluster_id":1,"topic_name":"わせ弁","topic_id":1,"topic_score":90,"period_from":1412121600,"period_to":1412726400,"period_id":1}]}'
//alert(json_text);


//json形式のファイルの読み込み
//ファイル名 data_(日付)_(クラスタorトピック)_(クラスタid orトピックid)
d3.json("<?php echo $filename; ?>", function(data) {
//d3.json("data_1.json", function(data) {
  node = root = data;

  var nodes = pack.nodes(root);

  vis.selectAll("circle")
      .data(nodes)
    .enter().append("svg:circle")
      .attr("class", function(d) { return d.children ? "parent" : "child"; })
      .attr("cx", function(d) { return d.x; })
      .attr("cy", function(d) { return d.y; })
      .attr("r", function(d) { return d.r; })
      .on("click", function(d) { return zoom(node == d ? root : d); });

  vis.selectAll("text")
      .data(nodes)
    .enter().append("svg:text")
      .attr("class", function(d) { return d.children ? "parent" : "child"; })
      .attr("x", function(d) { return d.x; })
      .attr("y", function(d) { return d.y; })
      .attr("dy", ".35em")
      .attr("text-anchor", "middle")
      .style("opacity", function(d) { return d.r > 20 ? 1 : 0; })
      .text(function(d) { return d.topic_name; });

  d3.select(window).on("click", function() { zoom(root); });
});

function zoom(d, i) {
  var k = r / d.r / 2;
  x.domain([d.x - d.r, d.x + d.r]);
  y.domain([d.y - d.r, d.y + d.r]);

  var t = vis.transition()
      .duration(d3.event.altKey ? 7500 : 750);

  t.selectAll("circle")
      .attr("cx", function(d) { return x(d.x); })
      .attr("cy", function(d) { return y(d.y); })
      .attr("r", function(d) { return k * d.r; });

  t.selectAll("text")
      .attr("x", function(d) { return x(d.x); })
      .attr("y", function(d) { return y(d.y); })
      .style("opacity", function(d) { return k * d.r > 20 ? 1 : 0; });

  node = d;
  d3.event.stopPropagation();
}

    </script>
  </body>
</html>
